module.exports = function(bh) {
    bh.match('col', function(ctx, json) {
        let keys = ['', '-sm', '-md', '-lg', '-xl'];
        let cls = ctx.cls();
        if (Array.isArray(json.size)) {
            cls = json.size.map((size, index) => `col${keys[index]}-${size}`).concat(cls && cls.split(' '));
            ctx.cls(cls.join(' '), true);
        }
    });
};
