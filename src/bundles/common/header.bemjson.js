module.exports = (type) => [
  {block: 'header', mods: {type: type}, content: [
    {cls: 'container', content: [
      {elem: 'row', cls: 'row', content: [
        {elem: 'col', cls: 'col-6 col-md-12 col-lg-2 mb-md-2 mb-lg-0', content: [
          {elem: 'logo', width: '100'},
        ]},
        {elem: 'col', cls: 'col-6 col-md-12 col-lg-10', content: [
          require('./navigation.bemjson'),
        ]},
      ]},
    ]},
  ]},
];
