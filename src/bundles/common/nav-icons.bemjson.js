module.exports = [
    {block: 'nav-icons', cls: 'font-size-lg', content: [
        {block: 'fi', mods: {icon: 'fb'}},
        {block: 'fi', mods: {icon: 'instagram'}},
        {block: 'fi', mods: {icon: 'vk'}},
        {block: 'fi', mods: {icon: 'ok'}},
    ]},
];
