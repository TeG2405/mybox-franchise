const ITEMS = [
    'Новости MyBox',
    'Карта процессов',
    'Обучение',
    'Маркетинг',
];
module.exports = [
    {block: 'navigation', content: [
        {elem: 'control'},
        {elem: 'collapse', cls: 'fade', content: [
            {elem: 'nav', content: ITEMS.map((item) => [
                {elem: 'item', content: [
                    {elem: 'link', content: item},
                ]},
            ])},
        ]},
    ]},
];