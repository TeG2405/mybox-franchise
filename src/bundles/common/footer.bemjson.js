const NAVS = [
  'Новости MyBox',
  'Обучение',
  'Карта процессов',
  'Маркетинг',
];
module.exports = [
  {block: 'footer', content: [
    {cls: 'container', content: [
      {cls: 'row', content: [
        {cls: 'col-12 col-sm-4 col-md-3', content: [
          {elem: 'logo', width: '140'},
        ]},
        {cls: 'col-12 col-sm-8 col-md-6 col-lg-5 col-xl-6 mt-3 mt-sm-0', content: [
          {elem: 'nav', content: NAVS.map((item)=>[
            {tag: 'li', content: [
              {block: 'a', content: item},
            ]},
          ])},
        ]},
        {cls: 'col-12 col-sm-8 col-md-3 col-lg-4 col-xl-3 mt-3 mt-md-0 text-white', content: [
          require('./social.bemjson'),
        ]},
      ]},
      {elem: 'copyright', cls: 'row mt-3', content: [
        {cls: 'col-12 col-lg-8 col-xl-9 py-s', content: [
          {tag: 'span', cls: 'text-white', content: [
            '© 2013-2018',
          ]},
        ]},
        {cls: 'col-12 col-lg-4 col-xl-3 py-s', content: [
          require('./developer.bemjson'),
        ]},
      ]},
    ]},
  ]},
  require('./modals.bemjson'),
];
