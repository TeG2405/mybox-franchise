const ITEMS = 15;
const ACTIVE = 3;
module.exports = [
    {block: 'swiper-page-pagination', cls: 'd-flex', attrs: {'data-active': ACTIVE}, content: [
        {elem: 'button', mods: {prev: true}, content: [
            {block: 'a', cls: 'page-link m-0 mr-s', mix: {block: 'fi', mods: {icon: 'angle-left'}}},
        ]},
        {cls: 'swiper-container swiper-container-horizontal', content: [
            {cls: 'swiper-wrapper', content: [
                new Array(ITEMS).fill('').map((item, index) => [
                    {cls: index + 1 == ACTIVE ? 'swiper-slide w-auto page-item active' : 'swiper-slide w-auto page-item', content: [
                        {block: 'a', cls: 'page-link mx-s', content: index + 1},
                    ]},
                ]),
            ]},
        ]},
        {elem: 'button', mods: {next: true}, content: [
            {block: 'a', cls: 'page-link ml-s', mix: {block: 'fi', mods: {icon: 'angle-right'}}},
        ]},
    ]},
];
