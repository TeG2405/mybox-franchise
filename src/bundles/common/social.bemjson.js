module.exports = [
    {block: 'social', content: [
        {elem: 'title', elemMods: {variant: 'smile'}, content: [
            {tag: 'span', cls: 'text-orange', content: 'MY'},
            ' В соц сетях',
        ]},
        {elem: 'nav', content: [
            {block: 'fi', mods: {icon: 'fb'}},
            {block: 'fi', mods: {icon: 'instagram'}},
            {block: 'fi', mods: {icon: 'vk'}},
            {block: 'fi', mods: {icon: 'ok'}},
        ]},
    ]},
];
