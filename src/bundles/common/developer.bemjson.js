module.exports = [
    {block: 'developer', content: [
        {block: 'a', cls: 'align-middle', attrs: {href: 'https://www.intervolga.ru'}, content: 'Разработка сайта'},
        {tag: 'span', cls: 'align-middle', content: ' — '},
        {elem: 'logo', tag: 'span', cls: 'align-middle'},
    ]},
];
