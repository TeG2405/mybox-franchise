module.exports = [
    {block: 'card-news', content: [
        {elem: 'image', content: [
            {block: 'image', mods: {size: '600x300'}, cls: 'text-center', content: [
                {block: 'img', mods: {lazy: true}, src: 'upload/card-news.jpg'},
            ]},
        ]},
        {elem: 'caption', content: [
            {elem: 'date', content: '04.11.18'},
            {elem: 'title', content: 'WELCOME TO MYBOXTEAM'},
            {elem: 'description', content: [
                {tag: 'p', content: 'В конце каждого месяца ты можешь познакомиться с компанией поближе. Для этого просто нужно поучаствовать в Welcome - тренинге, где мы расскажем тебе обо всём.'}
            ]},
            {elem: 'more', content: [
                {tag: 'span', cls: 'pr-3 align-middle text-uppercase font-size-md', content: 'подробнее'},
                {block: 'fi', tag: 'small', cls: 'align-middle', mods: {icon: 'arrow-long-right'}},
            ]},
        ]},
    ]},
];
