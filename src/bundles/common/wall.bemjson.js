module.exports = [
    {block: 'wall', cls: 'mb-5', content: [
        {elem: 'background', content: [
            {elem: 'image', width: 2088, height: 594, src: 'images/wall.jpg'},
        ]},
        {elem: 'inner', cls: 'fade show d-md-block', attrs: {id: 'HIDDEN_IS_NAVIGATION_OPEN'}, content: [
            {elem: 'container', cls: 'container', content: [
                {elem: 'title', content: 'Welcome'},
                {elem: 'description', content: [
                    {elem: 'list', content: [
                        'Тренинг для новых сотрудников!',
                    ]},
                ]},
            ]},
        ]},
    ]},
];
