module.exports = {
    block: 'page',
    title: 'Главная',
    content: [
        require('./common/header.bemjson')('sticky'),
        {block: 'main', content: [
            require('./common/wall.bemjson'),
            {cls: 'container', content: [
                {block: 'section', cls: 'mb-4', content: [
                    {elem: 'title', elemMods: {variant: 'smile'}, content: [
                        {tag: 'span', cls: 'text-orange', content: 'MY'},
                        ' АКТИВНОСТЬ',
                    ]},
                    {cls: 'row', content: new Array(4).fill([
                        {cls: 'col-12 col-md-6 py-l', content: require('./common/card-news.bemjson')},
                    ])},
                    {cls: 'py-l mt-3', content: [
                        require('./common/swiper-page-pagination.bemjson'),
                    ]},
                ]},
                {block: 'section', cls: 'mb-4', content: [
                    {elem: 'title', elemMods: {variant: 'smile'}, content: [
                        {tag: 'span', cls: 'text-orange', content: 'MY'},
                        ' ВОПРОСЫ И ОТВЕТЫ',
                    ]},
                    {cls: 'row', content: new Array(4).fill([
                        {cls: 'col-12 py-l', content: require('./common/quote.bemjson')},
                    ])},
                    {cls: 'py-l mt-m', content: [
                        require('./common/swiper-page-pagination.bemjson'),
                    ]},
                ]},
            ]},
        ]},
        require('./common/footer.bemjson'),
    ],
};
