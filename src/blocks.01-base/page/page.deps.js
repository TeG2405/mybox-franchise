module.exports = {
    mustDeps: [
        {block: 'utilities'},
    ],
    shouldDeps: [
        {elem: ['alert', 'ua']},
    ],
};
