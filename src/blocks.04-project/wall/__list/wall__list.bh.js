module.exports = function (bh) {
    bh.match('wall__list', function (ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                if(ctx.isSimple(item)) return {tag: 'li', content: item};
                return item
            }),
            true
        );
    });
};
