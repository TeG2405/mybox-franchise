$('.developer').on('click', (e) => {
    let $link = $(e.target).find('a');
    if ($link.length) {
        window.location.href = $link.attr('href');
    }
});
