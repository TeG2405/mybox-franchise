(($)=>{
    const EVENT = {
        SHOW: 'show.iv.open',
        HIDE: 'hide.iv.open',
    };
    const Selectors = {
        'HIDDEN_IS_NAVIGATION_OPEN': '#HIDDEN_IS_NAVIGATION_OPEN',
    };
    $(document)
        .on(EVENT.HIDE, (event) => {
            $(Selectors.HIDDEN_IS_NAVIGATION_OPEN).addClass('show');
        })
        .on(EVENT.SHOW, (event) => {
            $(Selectors.HIDDEN_IS_NAVIGATION_OPEN).removeClass('show');
        });
})($);

