module.exports = function (bh) {
    bh.match('navigation__control', function (ctx, json) {
        ctx
            .tag('button')
            .attrs({
                'type': 'button',
                'data-toggle': 'open',
                'data-target': `#${ctx.tParam('ID')}`,
            })
            .content(new Array(3).fill({tag: 'i'}));
    });
};
