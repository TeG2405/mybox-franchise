module.exports = function(bh) {
    bh.match('nav-icons', function(ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item) => [
                {elem: 'item', content: [
                    {elem: 'link', content: item},
                ]},
            ]),
            true
        );
    });
};
